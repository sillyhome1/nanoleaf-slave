# Sillyhome - Nanoleaf Slave



## The Problem:

Nanoleaf produces smart home light panels that can be integrated in several "smart" home kits.
However "smart" home kits are - like every other thing that used to be "smart" - often not very privacy-conscious.
Most modern IOT devices are very intrusive into the privacy of their users. Nanoleaf isn't an exception.


## The Solution:

Sillyhome nanoleaf-slave is an open source integration of nanoleaf devices like Panels to control them completely
offline. The idea is to seperate these nanoleaf devices from the rest of the network using a raspberry pi running
this slave inside a DMZ. It is also possible to syncronize the nanoleaf devices together or with the
[raspberrypi LED strip](https://gitlab.com/sillyhome1/raspberrypi-led-strip).
With Sillyhome nanoleaf-slave it is possible to use the nanoleaf devices completely privacy-friendly.

Sillyhome currently supports Light Panels, Canvas and Shapes.

## Get started:

To install or remove this slave on your raspberry pi as a service just run the `install.py` inside the `/installer`
folder.

To run this slave manually in the terminal (for example for debugging) run `main.py --start`.

## What does this do?:

This is just a slave! It will interact with the [LED manager](https://gitlab.com/sillyhome1/led-management) in the network.
So make sure that a LED manager server is up and running on the network as well.
This slave represents all nanoleaf devices in the network (DMZ).