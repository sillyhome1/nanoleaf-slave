import _thread
from http.server import HTTPServer
from functools import partial
from unittest.mock import Mock
from unittest import TestCase
import requests

from network.http_server import ServerHandler


class HTTPServerTest(TestCase):
    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)
        self.exampleEffect = {
            "infos": {"effect_group": "static-color", "effect_value": "baDa55"}
        }

    def test_do_POST(self):
        """
        Tests one successful and one failed POST.
        """
        mockNanoleaf = Mock()
        mockAttributes = {"postEffect.return_value": True}
        mockNanoleaf.configure_mock(**mockAttributes)

        handler = partial(ServerHandler, mockNanoleaf)
        server = HTTPServer(("127.0.0.1", 0), handler)
        _thread.start_new_thread(server.serve_forever, ())

        _, port = server.server_address
        url = f"http://127.0.0.1:{port}/nanoleaf"
        try:
            response = requests.post(url=url, json=self.exampleEffect, timeout=10)
        except Exception as e:
            server.shutdown()
            raise e

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["title"], "OK")

        mockNanoleaf.reset_mock()
        mockAttributes = {"postEffect.return_value": False}
        mockNanoleaf.configure_mock(**mockAttributes)

        try:
            response = requests.post(url=url, json=self.exampleEffect, timeout=10)
        except Exception as e:
            server.shutdown()
            raise e

        self.assertEqual(response.status_code, 500)
        self.assertEqual(response.json()["title"], "The request failed")

        server.shutdown()

    def test_do_GET(self):
        """
        Tests one successful and one failed GET.
        """
        mockNanoleaf = Mock()
        mockAttributes = {"getEffect.return_value": self.exampleEffect}
        mockNanoleaf.configure_mock(**mockAttributes)

        handler = partial(ServerHandler, mockNanoleaf)
        server = HTTPServer(("127.0.0.1", 0), handler)
        _thread.start_new_thread(server.serve_forever, ())

        _, port = server.server_address
        url = f"http://127.0.0.1:{port}/nanoleaf"

        try:
            response = requests.get(url=url, timeout=10)
        except Exception as e:
            server.shutdown()
            raise e

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["infos"]["effect_group"], "static-color")

        mockNanoleaf.reset_mock()
        mockAttributes = {"getEffect.return_value": {}}
        mockNanoleaf.configure_mock(**mockAttributes)

        try:
            response = requests.get(url=url, timeout=10)
        except Exception as e:
            server.shutdown()
            raise e

        self.assertEqual(response.status_code, 500)
        self.assertEqual(response.json()["title"], "The request failed")

        server.shutdown()
