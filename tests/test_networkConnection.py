from unittest import TestCase
from unittest.mock import patch
from network.connection import NetworkConnection

import subprocess


class TestNetworkConnection(TestCase):
    """
    Testing the NetworkConnection class
    """

    networkConnection = NetworkConnection()

    def test_findMyDevices(self) -> None:
        """
        Testing that NetworkConnection.findMyDevices will throw an error if there are no interfaces or no cidr

        :return: None
        """

        with patch.object(self.networkConnection, '_getNetworkInterfaces') as _getNetworkInterfacesMock:
            _getNetworkInterfacesMock.return_value = []
            with self.assertRaises(LookupError) as rte:
                self.networkConnection.findMyDevices()

            # Just mock a random IP to continue
            _getNetworkInterfacesMock.return_value = ['192.168.283.15']
            with patch.object(self.networkConnection, '_getCIDRs') as _getCIDRsMock:
                _getCIDRsMock.return_value = []
                with self.assertRaises(LookupError) as rte:
                    self.networkConnection.findMyDevices()
                _getCIDRsMock.assert_called_once()


    def test__getNetworkInterfaces(self) -> None:
        """
        Testing that NetworkConnection._getNetworkInterfaces will return all interfaces of the system

        :return: None
        """

        with patch('subprocess.check_output') as checkOutputMock:
            checkOutputMock.return_value = b'enp38s0\nlo\nwlo1\n'
            assert self.networkConnection._getNetworkInterfaces() == ['enp38s0', 'lo', 'wlo1']


    def test__getCIDRs(self) -> None:
        """
        Testing that NetworkConnection._getCIDRs will get the CIDRs of the interfaces and filter out localhost

        :return: None
        """

        with patch('subprocess.check_output') as checkOutputMock:
            # Check filtering out localhost
            checkOutputMock.return_value = b'inet 127.0.0.1/24'
            assert self.networkConnection._getCIDRs(['lo']) == []

            # Check raise error
            checkOutputMock.side_effect = subprocess.CalledProcessError(1, ['systemctl'])
            assert self.networkConnection._getCIDRs(['lo']) == []

            # Check valid interface
            checkOutputMock.side_effect = None
            checkOutputMock.return_value = b'inet 13.37.13.69/24'
            assert self.networkConnection._getCIDRs(['lo']) == ['13.37.13.69/24']


    def test__checkInterfaces(self) -> None:
        """
        Testing that NetworkConnection._checkInterfaces will spawn the pool correct and will filter all empty addresses

        :return: None
        """

        with patch("network.connection.Pool") as poolMock:
            # the Class "Pool" was just patched
            # this patches instantiated Objects of that Class with itself
            poolMock.return_value = poolMock

            poolMock.map.return_value = [['13.37.13.69']]
            # these magick methods are required in a context manager
            poolMock.__enter__.return_value = poolMock
            poolMock.__exit__.return_value = True

            ipAddresses = self.networkConnection._checkInterfaces(['13.37.13.37/24'])
            assert ipAddresses == ['13.37.13.69']
