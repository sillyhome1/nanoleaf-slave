# Contributing to sillyhome

First off, thank you for taking some of your time to contribute!

Any assistance is welcome! Please make sure to read this file and consider the instructions before contributing. 
This makes it a lot easier for us maintainers to implement an issue or review your code.

## Philosophy

The following are some fundamental principles for coding:

- Be explicit, never implicit!
- The simples way is the right way
- Faster code is better than slow code
- Clearer code before faster code though
- A function should do exactly one job
- A class/object does have only one purpose
- All parts of the unix philosophy (that's why this slave does exist in the first place)
- Open source is only really open source if the code is easy to understand for the uninvolved

## Coding style

If you understand and consider the philosophy of this project you are on a good way!
There are some things to know about to realize the philosophy:

- Every function/method must have verbose type hinting. Please use type hinting for python < 3.10!
It should look like this:
```python
def someFunction(self, someDict: dict[str: str]) -> Union[str, None]: ...
```
- Every function must have a docstring containing an explanation what the function is doing (what, not how!),
a description of every incoming and outgoing variable/data and a description for every error that is thrown
in the function:
```python
def someFunction(self, someDict: dict[str: str]) -> Union[str, None]:
    """
    Showing how a function should be documented
    
    :param someDict: An example dict containing some example data
    :raises ValueError: If the validation of someDict fails
    :return: Either the first key of the example dict or None if no key exists
    """
    ...
```
- If a location in the code is not trivial OR may be confusing for others please make a comment.
You should ask yourself: will I understand this code section in a year? Sometimes it is clear what the code is doing
but not why!


## Workflow

Every bug/feature will be recorded in an issue. The issue should describe the problem (the "is-state") and
how to solve the problem (target-state). Every issue should be assigned to a milestone.

Merge requests that fix a problem described in an issue should be named like this:
`$issueId-short-description-of-implementation` so it is easy to understand what merge request is related to an issue.
