import json
import logging
import urllib.parse
from http import HTTPStatus
from http.server import BaseHTTPRequestHandler

# TODO: this needs to be changed when the nanoleaf object is actually implemented
from network.nanoleaf_mock import nanoleafObject


class ServerHandler(BaseHTTPRequestHandler):
    """
    Server handler to handle HTTP requests from a management server.
    It responds to HTTP GET and POST requests.
    All responses conform to RFC-7807.

    For identification of nanoleaf devices it uses the first string after the first "/" in the URI.
        eg. the URI "192.168.0.69/1db99e62-8bb8" has the identifier "1db99e62-8bb8"

    The response to a GET request is the currently running effect for the nanoleaf device specified in the URI.

    A POST request additionally includes a JSON Object in the request body.
    This Object contains a LED effect that needs to be applied to the specified nanoleaf.

    Args:
        BaseHTTPRequestHandler (_type_): _description_
    """

    def __init__(
        self,
        # TODO: this needs to be changed when the nanoleaf object is actually implemented
        nanoleaf: nanoleafObject,
        *args,
        **kwargs,
    ) -> None:
        self.nanoleaf = nanoleaf

        super().__init__(*args, **kwargs)

    def log_message(self, format: str, *args):
        """
        Overwrites the Parents log_message handler.
        This would normally log to STDOUT.
        We do not want any of the HTTP Servers default log entries.
        This is why we silence the default logging.

        Args:
            format (str): desired format of logs (unused)
        """
        # mostly silence the terminal
        return

    def _setOKResponse(self) -> None:
        """
        Prepare most of the necessary items for a positive HTTP response (non-error).
        The actual Response body still needs to be filled.
        """
        self.send_response(HTTPStatus.OK)
        self.send_header("Content-type", "application/json")
        self.end_headers()

    def _setFailedResponse(self) -> None:
        """
        Perform a Server Error HTTP response.
        """
        self.send_response(HTTPStatus.INTERNAL_SERVER_ERROR)
        self.send_header("Content-type", "application/problem+json")
        self.end_headers()

        response = {
            "title": "The request failed",
            "detail": "The indicated nanoleaf might be unreachable or it responded with a invalid response.",
        }

        self.wfile.write(json.dumps(response).encode("utf-8"))

    def _setUnauthorizedResponse(self) -> None:
        """
        Perform a "unauthorized" HTTP response (error).
        """
        clientIP = self.client_address[0]
        logging.critical(f"unauthorized request from {clientIP}")

        self.send_response(HTTPStatus.UNAUTHORIZED)
        self.send_header("Content-type", "application/problem+json")
        self.end_headers()

        response = {
            "title": "You are unauthorized",
            "detail": "You are not authorized to perform this request.",
        }

        self.wfile.write(json.dumps(response).encode("utf-8"))

    def do_POST(self) -> None:
        """
        Overwrites the parents HTTP POST handler.

        This means that the managing Server is sending us a new
        led effect that needs to be displayed.
        """
        # read all available bytes from the HTTP Body
        # performing one read on the buffer
        # this is less blocking compared to rfile.read()
        postData = self.rfile.read1()

        slaveUri = self.path
        httpBody = urllib.parse.unquote_plus(postData.decode("utf-8"))
        clientIP = self.client_address[0]

        logging.debug(
            f"POST request for {slaveUri} with {httpBody} from {clientIP}"
        )

        if not self.isFromValidSource():
            self._setUnauthorizedResponse()
            return

        POST = json.loads(httpBody)

        success = self.nanoleaf.postEffect(POST, slaveUri)

        if not success:
            logging.debug(f"request failed with return code {success}")
            self._setFailedResponse()
            return

        logging.debug("request was successful")

        self._setOKResponse()

        response = {
            "title": "OK",
            "description": "The effect was successfully sent to the nanoleaf.",
        }

        self.wfile.write(json.dumps(response).encode("utf-8"))

    def do_GET(self) -> None:
        """
        Overwrites the parents HTTP GET handler.

        The response to a GET request is the currently running
        effect for the nanoleaf device specified in the URI.
        """
        clientIP = self.client_address[0]
        logging.debug(f"GET request for {self.path} from {clientIP}")

        if not self.isFromValidSource():
            self._setUnauthorizedResponse()
            return

        # the nanoleaf slave needs to respond with the currently running effect
        slaveURI = self.path
        currentEffect = self.nanoleaf.getEffect(slaveURI)

        if not currentEffect or not isinstance(currentEffect, dict):
            logging.debug(f"request failed with {currentEffect}")
            self._setFailedResponse()
            return

        logging.debug(f"device {slaveURI} has current effect {currentEffect}")

        self._setOKResponse()
        self.wfile.write(json.dumps(currentEffect).encode("utf-8"))

    def isFromValidSource(self) -> bool:
        """
        Check if the incoming request is coming from a valid Source.
        Only the led-manager that this device has registered
        itself to should be valid.

        Returns:
            bool: True if the incoming request is from a valid source
        """
        # TODO: implement authorization
        if False:
            logging.debug("request denied")
            return False
        logging.debug("request authorized")
        return True
