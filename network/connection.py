import subprocess

from nmap import PortScanner
from multiprocessing import Pool


class NetworkConnection:
    """
    Get the nanoleaf devices, provides some special methods to increase the chances to find the devices
    I'm not using the nanoleafapis discovery here because its poorly written
    """

    def findMyDevices (self) -> list[str]:
        """
        Check the network for nanoleaf devices

        THE MAC ADDRESS IS NOT IMPLEMENTED YET
        Sudo is required to get the mac address of any network device

        :return: A list containing the ip addresses of all nanoleaf devices found in the network
        :raises LookupError: If no network interface or no CIDR was found
        """

        # TODO get the MAC address of the nanoleaf devices to improve the performance if a devices is reconnected
        #  and has changed its ip address
        interfaces = self._getNetworkInterfaces()
        if not interfaces:
            raise LookupError('No network interfaces found!')
        cidrs = self._getCIDRs(interfaces)
        if not cidrs:
            raise LookupError('No CIDR found, cannot continue network scan!')

        return self._checkInterfaces(cidrs)


    def _getNetworkInterfaces (self) -> list[str]:
        """
        Get the interfaces of this system

        :return: A list of the active interfaces the system has
        """

        interfaces = subprocess.check_output('ls /sys/class/net', shell=True)
        # convert the bytes to string
        interfacesString = interfaces.decode('utf-8')
        # Remove the last \n to not get an empty list entry in the next line
        interfacesString = interfacesString[:-1]
        return interfacesString.split('\n')


    def _getCIDRs(self, interfaces: list) -> list[str]:
        """
        Get all netmask ip addresses (CIDR) the system has

        :param interfaces: A list of the active interfaces the system has
        :return: A list of CIDR (e.g. x.x.x.x/24)
        """

        cidrs = []

        for interface in interfaces:
            try:
                interfaceParams = str(subprocess.check_output(['ip', 'addr', 'show', interface]))
            except Exception:
                # Actually subprocess.CalledProcessError, but this error cannot be excepted
                # In case the interface is nonsense just ignore it, if cidrs is empty it
                # will raise an error in findMyDevices
                continue

            inet = interfaceParams[interfaceParams.find('inet') + 5:]
            cidr = inet[:inet.find(" ")]
            # filter out localhost
            if cidr and not cidr.startswith('127.'):
                cidrs.append(cidr)

        return cidrs


    def _checkInterfaces(self, cidrs: list[str]) -> list[str]:
        """
        Check the networks to get all ip addresses used in the networks using nmap

        :param cidrs: A list of CIDR (e.g. x.x.x.x/24)
        :return: A list of ip addresses which are probably nanoleaf devices
        """

        poolSize = len(cidrs)
        # Using 4 threads maximum
        poolSize = 4 if poolSize > 4 else poolSize

        with Pool(poolSize) as pool:
            ipAddressesList = pool.map(self._findAllAvailableAddresses, cidrs)

        # removing empty ip lists and merge all results into one list
        filteredIpAddresses = []
        for addresses in ipAddressesList:
            if addresses:
                filteredIpAddresses.extend(addresses)

        return filteredIpAddresses


    def _findAllAvailableAddresses(self, cidr: str) -> list[str]:
        """
        Scans the network for all ip addresses which are probably nanoleaf devices using nmap

        :param cidr: A single CIDR (x.x.x.x/24) to check the IP range
        :return: A list of ip addresses which are probably nanoleaf devices (empty list if nothing is found)
        """

        port = '16021'
        scanner = PortScanner()
        scanner.scan(cidr, port)
        # Filter for ip addresses with open port 16021 (which is the default API port for nanoleaf devices)
        checkPort = lambda host: any(
            [host[protocol].get(int(port), {}).get('state') == 'open' for protocol in host.all_protocols()])
        return [ip for ip in scanner.all_hosts() if checkPort(scanner[ip])]